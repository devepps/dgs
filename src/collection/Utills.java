package collection;

import java.util.ArrayList;
import java.util.Arrays;

import collection.copy.*;

public class Utills {

	public static <ContentType> ContentType[] concatArrays(ContentType[] first, @SuppressWarnings("unchecked") ContentType[]... rest) {
		int totalLength = first.length;
		for (ContentType[] array : rest) {
			totalLength += array.length;
		}
		ContentType[] result = Arrays.copyOf(first, totalLength);
		int pos = first.length;
		for (ContentType[] array : rest) {
			System.arraycopy(array, 0, result, pos, array.length);
			pos += array.length;
		}
		return result;
	}
	
	public static <ContentType> ArrayList<ContentType> cloneArrayList(ArrayList<ContentType> original, Cloner<ContentType> cloner) {
		if (original == null) {
			return null;
		}
		ArrayList<ContentType> copy = new ArrayList<>();
		for(int i = 0; i < original.size(); i++) {
			copy.add(cloner.clone(original.get(i)));
		}
		return copy;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <ContentType extends Copyable> ArrayList<ContentType> cloneArrayList(ArrayList<ContentType> original) {
		if (original == null) {
			return null;
		}
		ArrayList<ContentType> copy = new ArrayList<>();
		for(int i = 0; i < original.size(); i++) {
			Copyable originalObj = original.get(i);
			copy.add(originalObj == null ? null : (ContentType) originalObj.copy());
		}
		return copy;
	}

}
