package collection.copy;

public interface Cloner<ContentType> {

	public static final IntegerCloner INTEGER_CLONER = new IntegerCloner();
	public static final LongCloner LONG_CLONER = new LongCloner();
	public static final DoubleCloner DOUBLE_CLONER = new DoubleCloner();
	public static final StringCloner STRING_CLONER = new StringCloner();
	public static final BooleanCloner BOOLEAN_CLONER = new BooleanCloner();

	public static final class IntegerCloner implements Cloner<Integer> {
		@Override
		public Integer clone(Integer obj) {
			return obj == null ? null : obj.intValue();
		}
	}

	public static final class LongCloner implements Cloner<Long> {
		@Override
		public Long clone(Long obj) {
			return obj == null ? null : obj.longValue();
		}
	}

	public static final class DoubleCloner implements Cloner<Double> {
		@Override
		public Double clone(Double obj) {
			return obj == null ? null : obj.doubleValue();
		}
	}

	public static final class StringCloner implements Cloner<String> {
		@Override
		public String clone(String obj) {
			return obj == null ? null : obj + "";
		}
	}

	public static final class BooleanCloner implements Cloner<Boolean> {
		@Override
		public Boolean clone(Boolean obj) {
			return obj == null ? null : obj.booleanValue();
		}
	}

	public ContentType clone(ContentType obj);
}
