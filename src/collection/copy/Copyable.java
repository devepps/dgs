package collection.copy;

public interface Copyable<Supertype extends Copyable<?>> {

	public Supertype copy();

}
