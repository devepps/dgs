package collection.interceptor;

public interface InterceptedAction <ReturnType> {
	
	public ReturnType run();

}
