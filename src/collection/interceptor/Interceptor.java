package collection.interceptor;

public interface Interceptor<ReturnType> {
	
	public ReturnType intercept(InterceptedAction<ReturnType> run);

}