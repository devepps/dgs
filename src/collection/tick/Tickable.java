package collection.tick;

public interface Tickable {
	
	public void tick();

}
