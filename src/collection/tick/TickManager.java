package collection.tick;

public class TickManager {
	
	private int tickDuration;	
	private long ticks = 0;
	private long startTime = 0;
	
	private boolean run = true;
	
	private boolean released = false;
	public void release(){
		released = true;
		startTime = System.currentTimeMillis();
	}
	
	public TickManager(Tickable tickable, int tickDuration){
		this.tickDuration = tickDuration;
		
		new Thread(new Runnable(){
			@Override
			public void run() {
				startTime = System.currentTimeMillis();
				while(run){
					tickable.tick();
					int wait = (int) (tickDuration - getLatency()*tickDuration);
					if(!released)wait = 10;
					if(wait>0){
						synchronized (Thread.currentThread()) {
							try {
								Thread.currentThread().wait(wait);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}							
						}
					}
					if(released)ticks++;
				}
			}
		}).start();
	}

	public long getLatency() {
		return (System.currentTimeMillis()-startTime-(tickDuration*ticks))/tickDuration;
	}

	public long getCurrentTick() {
		return ticks;
	}

	public void kill() {
		run = false;
	}

}
