package collection.sync;

public class SyncManager {

	public interface ReturnableRunnable<ContentType> {
		public ContentType run();
	}

	public void syncronize(Runnable runnable) {
		this.syncronize(() -> {
			runnable.run();
			return null;
		});
	}

	public <ContentType> ContentType syncronize(ReturnableRunnable<ContentType> runnable) {
		synchronized (this) {
			try {
				ContentType o = runnable.run();
				return o;
			} catch (Throwable e) {
				throw e;
			}
		}
	}

	public void syncronizedWait(long millis) {
		this.syncronize(() -> {
			try {
				this.wait(millis);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		});
	}

	public void syncronizedWait(long millis, int nanaos) {
		this.syncronize(() -> {
			try {
				this.wait(millis, nanaos);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		});
	}

	public void asyncronizedWait(long millis) {
		try {
			Object obj = new Object();
			synchronized (obj) {
				obj.wait(millis);
			}
		} catch (Throwable e) {}
	}

	public void asyncronizedWait(long millis, int nanaos) {
		try {
			Object obj = new Object();
			synchronized (obj) {
				obj.wait(millis, nanaos);
			}
		} catch (Throwable e) {}
	}

}
