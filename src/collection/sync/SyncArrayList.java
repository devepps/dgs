package collection.sync;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class SyncArrayList<ContentType> extends ArrayList<ContentType> {

	private static final long serialVersionUID = -1887214313270403157L;
	private transient final SyncManager syncManager = new SyncManager();

	@Override
	public void trimToSize() {
		syncManager.syncronize(() -> super.trimToSize());
	}

	@Override
	public void ensureCapacity(int minCapacity) {
		syncManager.syncronize(() -> super.ensureCapacity(minCapacity));
	}

	@Override
	public int size() {
		return syncManager.syncronize(() -> super.size());
	}

	@Override
	public boolean isEmpty() {
		return syncManager.syncronize(() -> super.isEmpty());
	}

	@Override
	public boolean contains(Object o) {
		return syncManager.syncronize(() -> super.contains(o));
	}

	@Override
	public int indexOf(Object o) {
		return syncManager.syncronize(() -> super.indexOf(o));
	}

	@Override
	public int lastIndexOf(Object o) {
		return syncManager.syncronize(() -> super.lastIndexOf(o));
	}

	@Override
	public Object clone() {
		return syncManager.syncronize(() -> super.clone());
	}

	@Override
	public Object[] toArray() {
		return syncManager.syncronize(() -> super.toArray());
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return syncManager.syncronize(() -> super.toArray(a));
	}

	@Override
	public ContentType get(int index) {
		return syncManager.syncronize(() -> super.get(index));
	}

	@Override
	public ContentType set(int index, ContentType element) {
		return syncManager.syncronize(() -> super.set(index, element));
	}

	@Override
	public boolean add(ContentType e) {
		return syncManager.syncronize(() -> super.add(e));
	}

	@Override
	public void add(int index, ContentType element) {
		syncManager.syncronize(() -> super.add(index, element));
	}

	@Override
	public ContentType remove(int index) {
		return syncManager.syncronize(() -> super.remove(index));
	}

	@Override
	public boolean remove(Object o) {
		return syncManager.syncronize(() -> super.remove(o));
	}

	@Override
	public void clear() {
		syncManager.syncronize(() -> super.clear());
	}

	@Override
	public boolean addAll(Collection<? extends ContentType> c) {
		return syncManager.syncronize(() -> super.addAll(c));
	}

	@Override
	public boolean addAll(int index, Collection<? extends ContentType> c) {
		return syncManager.syncronize(() -> super.addAll(index, c));
	}

	@Override
	protected void removeRange(int fromIndex, int toIndex) {
		syncManager.syncronize(() -> super.removeRange(fromIndex, toIndex));
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return syncManager.syncronize(() -> super.removeAll(c));
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return syncManager.syncronize(() -> super.retainAll(c));
	}

	@Override
	public ListIterator<ContentType> listIterator(int index) {
		return syncManager.syncronize(() -> super.listIterator(index));
	}

	@Override
	public ListIterator<ContentType> listIterator() {
		return syncManager.syncronize(() -> super.listIterator());
	}

	@Override
	public Iterator<ContentType> iterator() {
		return syncManager.syncronize(() -> super.iterator());
	}

	@Override
	public List<ContentType> subList(int fromIndex, int toIndex) {
		return syncManager.syncronize(() -> super.subList(fromIndex, toIndex));
	}

	@Override
	public void forEach(Consumer<? super ContentType> action) {
		syncManager.syncronize(() -> super.forEach(action));
	}

	@Override
	public Spliterator<ContentType> spliterator() {
		return syncManager.syncronize(() -> super.spliterator());
	}

	@Override
	public boolean removeIf(Predicate<? super ContentType> filter) {
		return syncManager.syncronize(() -> super.removeIf(filter));
	}

	@Override
	public void replaceAll(UnaryOperator<ContentType> operator) {
		syncManager.syncronize(() -> super.replaceAll(operator));
	}

	@Override
	public void sort(Comparator<? super ContentType> c) {
		syncManager.syncronize(() -> super.sort(c));
	}

	@Override
	public boolean equals(Object o) {
		return syncManager.syncronize(() -> super.equals(o));
	}

	@Override
	public int hashCode() {
		return syncManager.syncronize(() -> super.hashCode());
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return syncManager.syncronize(() -> super.containsAll(c));
	}

	@Override
	public String toString() {
		return syncManager.syncronize(() -> super.toString());
	}

	@Override
	public Stream<ContentType> stream() {
		return syncManager.syncronize(() -> super.stream());
	}

	@Override
	public Stream<ContentType> parallelStream() {
		return syncManager.syncronize(() -> super.parallelStream());
	}

	public SyncManager getSyncManager() {
		return syncManager;
	}
}
