package collection.sync;

public interface SyncedDataUpdater<ContentType> {
	public ContentType update(ContentType currentContent);
}
