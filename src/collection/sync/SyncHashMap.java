package collection.sync;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class SyncHashMap<KeyType, ValueType> extends HashMap<KeyType, ValueType> {

	private static final long serialVersionUID = -8854942181349409788L;
	private transient final SyncManager syncManager = new SyncManager();
	private transient final UnSyncHashMap unSyncedHashMap = new UnSyncHashMap(this);

	@Override
	public int size() {
		return syncManager.syncronize(() -> super.size());
	}

	@Override
	public boolean isEmpty() {
		return syncManager.syncronize(() -> super.isEmpty());
	}

	@Override
	public ValueType get(Object key) {
		return syncManager.syncronize(() -> super.get(key));
	}

	@Override
	public boolean containsKey(Object key) {
		return syncManager.syncronize(() -> super.containsKey(key));
	}

	@Override
	public ValueType put(KeyType key, ValueType value) {
		return syncManager.syncronize(() -> super.put(key, value));
	}

	@Override
	public void putAll(Map<? extends KeyType, ? extends ValueType> m) {
		syncManager.syncronize(() -> super.putAll(m));
	}

	@Override
	public ValueType remove(Object key) {
		return syncManager.syncronize(() -> super.remove(key));
	}

	@Override
	public void clear() {
		syncManager.syncronize(() -> super.clear());
	}

	@Override
	public boolean containsValue(Object value) {
		return syncManager.syncronize(() -> super.containsValue(value));
	}

	@Override
	public Set<KeyType> keySet() {
		return syncManager.syncronize(() -> super.keySet());
	}

	@Override
	public Collection<ValueType> values() {
		return syncManager.syncronize(() -> super.values());
	}

	@Override
	public Set<java.util.Map.Entry<KeyType, ValueType>> entrySet() {
		return syncManager.syncronize(() -> super.entrySet());
	}

	@Override
	public ValueType getOrDefault(Object key, ValueType defaultValue) {
		return syncManager.syncronize(() -> super.getOrDefault(key, defaultValue));
	}

	@Override
	public ValueType putIfAbsent(KeyType key, ValueType value) {
		return syncManager.syncronize(() -> super.putIfAbsent(key, value));
	}

	@Override
	public boolean remove(Object key, Object value) {
		return syncManager.syncronize(() -> super.remove(key, value));
	}

	@Override
	public boolean replace(KeyType key, ValueType oldValue, ValueType newValue) {
		return syncManager.syncronize(() -> super.replace(key, oldValue, newValue));
	}

	@Override
	public ValueType replace(KeyType key, ValueType value) {
		return syncManager.syncronize(() -> super.replace(key, value));
	}

	@Override
	public ValueType computeIfAbsent(KeyType key, Function<? super KeyType, ? extends ValueType> mappingFunction) {
		return syncManager.syncronize(() -> super.computeIfAbsent(key, mappingFunction));
	}

	@Override
	public ValueType computeIfPresent(KeyType key,
			BiFunction<? super KeyType, ? super ValueType, ? extends ValueType> remappingFunction) {
		return syncManager.syncronize(() -> super.computeIfPresent(key, remappingFunction));
	}

	@Override
	public ValueType compute(KeyType key,
			BiFunction<? super KeyType, ? super ValueType, ? extends ValueType> remappingFunction) {
		return syncManager.syncronize(() -> super.compute(key, remappingFunction));
	}

	@Override
	public ValueType merge(KeyType key, ValueType value,
			BiFunction<? super ValueType, ? super ValueType, ? extends ValueType> remappingFunction) {
		return syncManager.syncronize(() -> super.merge(key, value, remappingFunction));
	}

	@Override
	public void forEach(BiConsumer<? super KeyType, ? super ValueType> action) {
		super.forEach((KeyType key, ValueType value) -> syncManager.syncronize(() -> action.accept(key, value)));
	}

	@Override
	public void replaceAll(BiFunction<? super KeyType, ? super ValueType, ? extends ValueType> function) {
		syncManager.syncronize(() -> super.replaceAll(function));
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<KeyType, ValueType> clone() {
		HashMap<KeyType, ValueType> copy = new HashMap<>();
		copy.putAll((Map<? extends KeyType, ? extends ValueType>) syncManager.syncronize(() -> super.clone()));
		return copy;
	}

	@Override
	public boolean equals(Object o) {
		return syncManager.syncronize(() -> super.equals(o));
	}

	@Override
	public int hashCode() {
		return syncManager.syncronize(() -> super.hashCode());
	}

	@Override
	public String toString() {
		return syncManager.syncronize(() -> super.toString());
	}

	public SyncManager getSyncManager() {
		return syncManager;
	}

	public UnSyncHashMap getUnsyncronized() {
		return unSyncedHashMap;
	}
	
	private int unsync_size() {
		return super.size();
	}

	private boolean unsync_isEmpty() {
		return super.isEmpty();
	}

	private ValueType unsync_get(Object key) {
		return super.get(key);
	}

	private boolean unsync_containsKey(Object key) {
		return super.containsKey(key);
	}

	private ValueType unsync_put(KeyType key, ValueType value) {
		return super.put(key, value);
	}

	private void unsync_putAll(Map<? extends KeyType, ? extends ValueType> m) {
		super.putAll(m);
	}

	private ValueType unsync_remove(Object key) {
		return super.remove(key);
	}

	private void unsync_clear() {
		super.clear();
	}

	private boolean unsync_containsValue(Object value) {
		return super.containsValue(value);
	}

	private Set<KeyType> unsync_keySet() {
		return super.keySet();
	}

	private Collection<ValueType> unsync_values() {
		return super.values();
	}

	private Set<java.util.Map.Entry<KeyType, ValueType>> unsync_entrySet() {
		return super.entrySet();
	}

	private ValueType unsync_getOrDefault(Object key, ValueType defaultValue) {
		return super.getOrDefault(key, defaultValue);
	}

	private ValueType unsync_putIfAbsent(KeyType key, ValueType value) {
		return super.putIfAbsent(key, value);
	}

	private boolean unsync_remove(Object key, Object value) {
		return super.remove(key, value);
	}

	private boolean unsync_replace(KeyType key, ValueType oldValue, ValueType newValue) {
		return super.replace(key, oldValue, newValue);
	}

	private ValueType unsync_replace(KeyType key, ValueType value) {
		return super.replace(key, value);
	}

	private ValueType unsync_computeIfAbsent(KeyType key, Function<? super KeyType, ? extends ValueType> mappingFunction) {
		return super.computeIfAbsent(key, mappingFunction);
	}

	private ValueType unsync_computeIfPresent(KeyType key,
			BiFunction<? super KeyType, ? super ValueType, ? extends ValueType> remappingFunction) {
		return super.computeIfPresent(key, remappingFunction);
	}

	private ValueType unsync_compute(KeyType key,
			BiFunction<? super KeyType, ? super ValueType, ? extends ValueType> remappingFunction) {
		return super.compute(key, remappingFunction);
	}

	private ValueType unsync_merge(KeyType key, ValueType value,
			BiFunction<? super ValueType, ? super ValueType, ? extends ValueType> remappingFunction) {
		return super.merge(key, value, remappingFunction);
	}

	private void unsync_forEach(BiConsumer<? super KeyType, ? super ValueType> action) {
		super.forEach(action);
	}

	private void unsync_replaceAll(BiFunction<? super KeyType, ? super ValueType, ? extends ValueType> function) {
		super.replaceAll(function);
	}

	private Object unsync_clone() {
		return super.clone();
	}
	
	public class UnSyncHashMap {
		
		private SyncHashMap<KeyType, ValueType> syncHashMap;
		
		public UnSyncHashMap(SyncHashMap<KeyType, ValueType> syncHashMap) {
			this.syncHashMap = syncHashMap;
		}

		public int size() {
			return syncHashMap.unsync_size();
		}

		public boolean isEmpty() {
			return syncHashMap.unsync_isEmpty();
		}

		public ValueType get(Object key) {
			return syncHashMap.unsync_get(key);
		}

		public boolean containsKey(Object key) {
			return syncHashMap.unsync_containsKey(key);
		}

		public ValueType put(KeyType key, ValueType value) {
			return syncHashMap.unsync_put(key, value);
		}

		public void putAll(Map<? extends KeyType, ? extends ValueType> m) {
			syncHashMap.unsync_putAll(m);
		}

		public ValueType remove(Object key) {
			return syncHashMap.unsync_remove(key);
		}

		public void clear() {
			syncHashMap.unsync_clear();
		}

		public boolean containsValue(Object value) {
			return syncHashMap.unsync_containsValue(value);
		}

		public Set<KeyType> keySet() {
			return syncHashMap.unsync_keySet();
		}

		public Collection<ValueType> values() {
			return syncHashMap.unsync_values();
		}

		public Set<java.util.Map.Entry<KeyType, ValueType>> entrySet() {
			return syncHashMap.unsync_entrySet();
		}

		public ValueType getOrDefault(Object key, ValueType defaultValue) {
			return syncHashMap.unsync_getOrDefault(key, defaultValue);
		}

		public ValueType putIfAbsent(KeyType key, ValueType value) {
			return syncHashMap.unsync_putIfAbsent(key, value);
		}

		public boolean remove(Object key, Object value) {
			return syncHashMap.unsync_remove(key, value);
		}

		public boolean replace(KeyType key, ValueType oldValue, ValueType newValue) {
			return syncHashMap.unsync_replace(key, oldValue, newValue);
		}

		public ValueType replace(KeyType key, ValueType value) {
			return syncHashMap.unsync_replace(key, value);
		}

		public ValueType computeIfAbsent(KeyType key, Function<? super KeyType, ? extends ValueType> mappingFunction) {
			return syncHashMap.unsync_computeIfAbsent(key, mappingFunction);
		}

		public ValueType computeIfPresent(KeyType key,
				BiFunction<? super KeyType, ? super ValueType, ? extends ValueType> remappingFunction) {
			return syncHashMap.unsync_computeIfPresent(key, remappingFunction);
		}

		public ValueType compute(KeyType key,
				BiFunction<? super KeyType, ? super ValueType, ? extends ValueType> remappingFunction) {
			return syncHashMap.unsync_compute(key, remappingFunction);
		}

		public ValueType merge(KeyType key, ValueType value,
				BiFunction<? super ValueType, ? super ValueType, ? extends ValueType> remappingFunction) {
			return syncHashMap.unsync_merge(key, value, remappingFunction);
		}

		public void forEach(BiConsumer<? super KeyType, ? super ValueType> action) {
			syncHashMap.unsync_forEach(action);
		}

		public void replaceAll(BiFunction<? super KeyType, ? super ValueType, ? extends ValueType> function) {
			syncHashMap.unsync_replaceAll(function);
		}

		public Object clone() {
			return syncHashMap.unsync_clone();
		}
	}

}
