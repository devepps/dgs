package collection.sync;

import data.queue.Action;
import data.queue.Queue;

public class SyncQueue<ContentType> extends Queue<ContentType> {

	private final transient SyncManager syncManager = new SyncManager();

	@Override
	public void add(ContentType Object) {
		syncManager.syncronize(() -> super.add(Object));
	}

	@Override
	public ContentType get() {
		return syncManager.syncronize(() -> super.get());
	}

	@Override
	public void remove() {
		syncManager.syncronize(() -> super.remove());
	}

	@Override
	public boolean isEmpty() {
		return syncManager.syncronize(() -> super.isEmpty());
	}

	@Override
	public Queue<ContentType> clone() {
		return syncManager.syncronize(() -> super.clone());
	}

	@Override
	public int getLength() {
		return syncManager.syncronize(() -> super.getLength());
	}

	@Override
	public ContentType pop() {
		return syncManager.syncronize(() -> super.pop());
	}

	@Override
	public void clear() {
		syncManager.syncronize(() -> super.clear());
	}
	
	@Override
	public void actOnContent(Action<ContentType> action) {
		super.actOnContent(action, (interceptorRunner) -> syncManager.syncronize(() -> interceptorRunner.run()));
	}

	public Queue<ContentType> clearAndClone() {
		return syncManager.syncronize(() -> {
			Queue<ContentType> copy = new Queue<>();
			while(!super.isEmpty()) {
				copy.add(super.pop());
			}
			return copy;
		});
	}

	public SyncQueue<ContentType> clearAndCloneSync() {
		return syncManager.syncronize(() -> {
			SyncQueue<ContentType> copy = new SyncQueue<>();
			while(!super.isEmpty()) {
				copy.add(super.pop());
			}
			return copy;
		});
	}

	public SyncManager getSyncManager() {
		return syncManager;
	}
}
