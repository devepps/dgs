package collection.sync;

public class SyncedData<ContentType> {
	
	private final SyncManager syncManager = new SyncManager();
	private ContentType content;

	public SyncedData(ContentType content) {
		this.content = content;
	}

	public SyncedData() {
	}

	public ContentType getContent() {
		return this.syncManager.syncronize(() -> this.content);
	}

	public void setContent(ContentType content) {
		this.syncManager.syncronize(() -> this.content = content);
	}

	public SyncManager getSyncManager() {
		return this.syncManager;
	}

	public ContentType update(SyncedDataUpdater<ContentType> updater) {
		return this.syncManager.syncronize(() -> {
			this.content = updater.update(this.content);
			return this.content;
		});
	}
}
