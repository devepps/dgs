package collection.sheduler;

import collection.sync.SyncManager;

public class Scheduler {

	private SchedulerTimer schedulerTimer;
	private boolean newThread;

	private boolean ended = false;

	public Scheduler(SchedulerTimer schedulerTimer, boolean newThread) {
		this.schedulerTimer = schedulerTimer;
		this.newThread = newThread;
	}

	public Scheduler shedule(Runnable run) {
		new Thread(new Runnable() {

			private final SyncManager syncManager = new SyncManager();
			private Thread currentThread = new Thread(run);

			@Override
			public void run() {
				while (!ended) {
					if (schedulerTimer.act()) {
						if (newThread) {
							currentThread = new Thread(run);
						}
						currentThread.start();
					}
					syncManager.syncronizedWait(0, 1);
				}
			}
		}).start();
		return this;
	}

}
