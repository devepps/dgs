package collection.sheduler;

import java.time.LocalTime;
import java.util.Arrays;

public class SchedulerTimer {

	private String[] acceptableDigits; // hh:mm:ss:xxx
	private LocalTime time = LocalTime.now();

	public SchedulerTimer(String config) {
		System.out.println("=== Timer Config ===");
		String[] units = config.split(":");
		this.acceptableDigits = new String[units.length * 2 + (units.length == 4 ? 1 : 0)];
		System.out.println(config + "->" + Arrays.toString(units));

		int index = 0;
		for (String unit : units) {
			unit = unit.replaceAll("[{}]", "");
			String[] digits = unit.split(",");
			for (String digit : digits) {
				acceptableDigits[index] = digit;
				index++;
			}
		}
	}

	public boolean act() {
		LocalTime time = LocalTime.now();
		int hour = time.getHour();
		int minutes = time.getMinute();
		int second = time.getSecond();
		int milliSecond = time.getNano() / 1000;

		int index = 0;
		if (acceptableDigits.length >= 2) {
			String h = String.format("%1$02d", hour);
			if (!check(h, index)) {
				return false;
			}
			index += 2;
		}
		if (acceptableDigits.length >= 4) {
			String m = String.format("%1$02d", minutes);
			if (!check(m, index)) {
				return false;
			}
			index += 2;
		}
		if (acceptableDigits.length >= 6) {
			String s = String.format("%1$02d", second);
			if (!check(s, index)) {
				return false;
			}
			index += 2;
		}
		if (acceptableDigits.length == 9) {
			String ms = String.format("%1$03d", milliSecond);
			if (!check(ms, index)) {
				return false;
			}
		}

		if (acceptableDigits.length == 2) {
			if (this.time.getHour() == hour) {
				return false;
			}
		} else if (acceptableDigits.length == 4) {
			if (this.time.getMinute() == minutes) {
				return false;
			}
		} else if (acceptableDigits.length == 6) {
			if (this.time.getSecond() == second) {
				return false;
			}
		} else if (acceptableDigits.length == 6) {
			if (this.time.getNano() / 1000 == milliSecond) {
				return false;
			}
		}

		this.time = LocalTime.now();
		return true;
	}

	private boolean check(String text, int index) {
		for (char c : text.toCharArray()) {
			if (!acceptableDigits[index].contains("*") && !acceptableDigits[index].contains(c + "")) {
				return false;
			}
			index++;
		}
		return true;
	}

}
