package collection.task;

public interface Task<Host>{

	public boolean act(Host host);

}
