package collection.task;

public class SuccessMessage {

	private boolean success;
	private String message;
	private Throwable error;

	public SuccessMessage(boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
	};

	public SuccessMessage(Throwable error) {
		super();
		this.success = false;
		this.message = "An error occured: " + error.getMessage();
		this.error = error;
	}

	public boolean isSuccessfully() {
		return success;
	}

	public String getMessage() {
		return message;
	}

	public Throwable getError() {
		return error;
	}
}
