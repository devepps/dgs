package collection.task;

import data.queue.Queue;

public class TaskManager<Host> {
	
	private Queue<Task<Host>> tasks = new Queue<>();

	public void addTask(Task<Host> task) {
		this.tasks.add(task);
	}
	
	public void tick(Host host) {
		while(!tasks.isEmpty()) {
			Task<Host> task = this.tasks.get();
			task.act(host);
			this.tasks.remove();
		}
	}

}
