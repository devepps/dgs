package config;

import java.io.File;
import java.util.ArrayList;

import fileManager.file.FileInterface;
import fileManager.file.ktv.KTVFileInterface;
import fileManager.file.ktv.data.Tree;
import fileManager.main.FileManager;

public class ConfigEnvironment {
	
	public static final String CONFIG_FILE_SUFFIX = "-properties";
	public static final String PRIORITY_PROPERTY = "priority";
	public static final int DEFAULT_PRIORITY = -1;
	
	private static final ConfigEnvironment CONFIG_ENVIRONMENT = new ConfigEnvironment();
	
	private FileManager fileManager;
	private Tree<String> properties = new Tree<String>();
	
	private ConfigEnvironment() {
		this.fileManager = new FileManager();
		loadSubFiled("config");
	}

	private void loadSubFiled(String path) {
		loadConfig(path);
		File base = new File(path);
		for(File f : base.listFiles()) {
			if (f.isDirectory()) {
				loadConfig(path + "/" + f.getName());
			}
		}
	}

	private void loadConfig(String path) {
		ArrayList<FileInterface<?, ?>> configFiles = fileManager.getFiles(path);
		configFiles.stream()
				.filter(configFile -> (configFile instanceof KTVFileInterface) && configFile.getName().endsWith(CONFIG_FILE_SUFFIX))
				.forEach(configFile -> configFile.load());
		configFiles.stream()
				.filter(configFile -> (configFile instanceof KTVFileInterface) && configFile.getName().endsWith(CONFIG_FILE_SUFFIX))
				.sorted((configFile1, configFile2) -> {
					int priority1 = getPriority((KTVFileInterface) configFile1);
					int priority2 = getPriority((KTVFileInterface) configFile2);
					return priority1 == priority2 ? 0 : ((priority1 > priority2) ? 1 : -1);
				}).forEach(configFile -> readConfig((KTVFileInterface) configFile, ""));
	}

	private void readConfig(KTVFileInterface configFile, String key) {
		ArrayList<String> properties = configFile.get(key);
		if(properties != null) {
			properties.stream().filter(propertie -> propertie != null).forEach(propertie -> this.properties.add(key, propertie));
		}
		for(String subKey: configFile.getSubkey(key)) {
			readConfig(configFile, key + (key.equals("") ? "" : ".") + subKey);
		}
	}

	private int getPriority(KTVFileInterface configFile1) {
		ArrayList<String> priorityData = configFile1.get(PRIORITY_PROPERTY);
		if(priorityData == null || priorityData.size() == 0 || priorityData.get(0) == null) {
			return DEFAULT_PRIORITY;
		}
		try {
			return Integer.parseInt(priorityData.get(0));
		} catch (NumberFormatException e) {
			return DEFAULT_PRIORITY;
		}
	}
	
	public static String getProperty(String key) {
		ArrayList<String> properties = CONFIG_ENVIRONMENT.properties.get(key);
		if(properties == null || properties.size() == 0 || properties.get(0) == null) {
			return "";
		}
		return properties.get(0);
	}
	
	public static ArrayList<String> getProperties(String key) {
		ArrayList<String> properties = CONFIG_ENVIRONMENT.properties.get(key);
		return properties == null ? new ArrayList<>() : properties;
	}
	
	public static String[] getSubProperties(String key) {
		return CONFIG_ENVIRONMENT.properties.getSubKey(key);
	}
	
	public static int getPropertyInt(String key) {
		return Integer.parseInt(getProperty(key));
	}
	
	public static ArrayList<Integer> getPropertiesInt(String key) {
		ArrayList<Integer> data = new ArrayList<>();
		for(String config : getProperties(key)) {
			data.add(Integer.parseInt(config));
		}
		return data;
	}
}
