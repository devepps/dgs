package fileManager.test;

import java.awt.Point;

import fileManager.file.any.AnyFileInterface;
import fileManager.file.csv.CSVFileInterface;
import fileManager.file.ktv.KTVFileInterface;
import fileManager.file.lt.LTFileInterface;
import fileManager.main.FileManager;

public class FileTest {
	
	public static void main(String[] args) {
		new FileTest();
	}
	
	private FileManager fileManager = new FileManager();
	
	public FileTest() {
		KTVFileInterface ktvFile = fileManager.getKTVFile("data", "ktvTest");
		CSVFileInterface csvFile = fileManager.getCSVFile("data", "csvTest");
		AnyFileInterface anyFile = fileManager.getAnyFile("data", "anyTest", "data");
		
		ktvFile.load();
		csvFile.load();
		anyFile.load();
		
//		ktvFile.add("abc.test.bob", "Test1", "Test2", "Test3");
//		ktvFile.add("abc", "Test4", "Test5");
//		ktvFile.add("abc.test2.bob", "Test6", "Test7");
//		
//		csvFile.set(new Point(0, 0), "Test1");
//		csvFile.set(new Point(1, 0), "Test2");
//		csvFile.set(new Point(0, 1), "Test3");
//		csvFile.set(new Point(1, 1), "Test4");
//		csvFile.set(new Point(4, 3), "Test5");
//		
//		anyFile.setData("test".getBytes());
		
		ktvFile.save();
		csvFile.save();
		anyFile.save();
		
		KTVFileInterface ktvEncryptedFile = fileManager.getEncryptedKTVFile("data", "ktvTestEN", "pw1");
		CSVFileInterface csvEncryptedFile = fileManager.getEncryptedCSVFile("data", "csvTestEN", "pw2");
		AnyFileInterface anyEncryptedFile = fileManager.getEncryptedAnyFile("data", "anyTestEN", "data", "pw3");
		
//		ktvEncryptedFile.add("abc.test.bob", "Test1", "Test2", "Test3");
//		ktvEncryptedFile.add("abc", "Test4", "Test5");
//		ktvEncryptedFile.add("abc.test2.bob", "Test6", "Test7");
//		
//		csvEncryptedFile.set(new Point(0, 0), "Test1");
//		csvEncryptedFile.set(new Point(1, 0), "Test2");
//		csvEncryptedFile.set(new Point(0, 1), "Test3");
//		csvEncryptedFile.set(new Point(1, 1), "Test4");
//		csvEncryptedFile.set(new Point(4, 3), "Test5");
//		
//		anyEncryptedFile.setData("test-Bytes".getBytes());

		ktvEncryptedFile.load();
		csvEncryptedFile.load();
		anyEncryptedFile.load();
		
		System.out.println("KTV-Success: " + (ktvEncryptedFile.get("abc.test.bob").size() == 3));
		System.out.println("CSV-Success: " + (csvEncryptedFile.get(new Point(1, 0)).equals("Test2")));
		System.out.println("Any-Success: " + (new String(anyEncryptedFile.getData()).equals("test-Bytes") + "-> \"" + new String(anyEncryptedFile.getData()) + "\""));
		
		ktvEncryptedFile.save();
		ktvEncryptedFile.save();
		csvEncryptedFile.save();
		csvEncryptedFile.save();
		anyEncryptedFile.save();
		
		
		LTFileInterface ltFile = fileManager.getLTFile("data", "ltTest");
		
		ltFile.load();
		
		ltFile.write("test");
		ltFile.write("- ");
		ltFile.writeln("test2");
		ltFile.writeln("test3");
		ltFile.newLine();
		ltFile.newLine();
		ltFile.newLine();
		ltFile.write("test4");
		
		ltFile.save();
		ltFile.save();
		ltFile.save();
	}

}
