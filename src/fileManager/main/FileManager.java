package fileManager.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import fileManager.data.exceptions.FileCreationException;
import fileManager.file.FileInterface;
import fileManager.file.FileReader;
import fileManager.file.any.*;
import fileManager.file.csv.*;
import fileManager.file.ktv.*;
import fileManager.file.lt.LTFile;
import fileManager.file.lt.LTFileInterface;
import fileManager.file.lt.LTReader;
public class FileManager {
	
	private HashMap<String, FileReader<?>> files = new HashMap<>();
	
	public CSVFileInterface getCSVFile(String path, String name) {		
		try {
			File f = new File(path);
			path = f.getCanonicalPath();
		} catch (IOException e) {
			throw new FileCreationException(e, "IO-Exception");
		}

		if (files.containsKey(path + "/" + name + ".csv")) {
			FileReader<?> reader = files.get(path + "/" + name + ".csv");
			if (reader instanceof CSVReader) {
				return new CSVFileInterface(((CSVReader)reader).getFile(), (CSVReader) reader);
			} else {
				throw new FileCreationException("File as non CSV-File known");					
			}
		} else {
			CSVFile file = new CSVFile(name, path);
			CSVReader reader = new CSVReader(file);
			
			files.put(path + "/" + name + ".csv", reader);
			return new CSVFileInterface(file, reader);
		}		
	}
	
	public KTVFileInterface getKTVFile(String path, String name) {		
		try {
			File f = new File(path);
			path = f.getCanonicalPath();
		} catch (IOException e) {
			throw new FileCreationException(e, "IO-Exception");
		}

		if (files.containsKey(path + "/" + name + ".ktv")) {
			FileReader<?> reader = files.get(path + "/" + name + ".ktv");
			if (reader instanceof KTVReader) {
				return new KTVFileInterface(((KTVReader)reader).getFile(), (KTVReader) reader);
			} else {
				throw new FileCreationException("File as non KTV-File known");					
			}
		} else {
			KTVFile file = new KTVFile(name, path);
			KTVReader reader = new KTVReader(file);
			
			files.put(path + "/" + name + ".ktv", reader);
			return new KTVFileInterface(file, reader);
		}		
	}

	public LTFileInterface getLTFile(String path, String name) {
		try {
			File f = new File(path);
			path = f.getCanonicalPath();
		} catch (IOException e) {
			throw new FileCreationException(e, "IO-Exception");
		}

		if (files.containsKey(path + "/" + name + ".ktv")) {
			FileReader<?> reader = files.get(path + "/" + name + ".ktv");
			if (reader instanceof KTVReader) {
				return new LTFileInterface(((LTReader)reader).getFile(), (LTReader) reader);
			} else {
				throw new FileCreationException("File as non KTV-File known");					
			}
		} else {
			LTFile file = new LTFile(name, path);
			LTReader reader = new LTReader(file);
			
			files.put(path + "/" + name + ".ktv", reader);
			return new LTFileInterface(file, reader);
		}		
	}
	
	public AnyFileInterface getAnyFile(String path, String name, String type) {		
		try {
			File f = new File(path);
			path = f.getCanonicalPath();
		} catch (IOException e) {
			throw new FileCreationException(e, "IO-Exception");
		}

		if (files.containsKey(path + "/" + name + "." + type)) {
			FileReader<?> reader = files.get(path + "/" + name + "." + type);
			if (reader instanceof AnyReader && reader.getFile().getType().equals(type)) {
				return new AnyFileInterface(((AnyReader)reader).getFile(), (AnyReader) reader);
			} else {
				throw new FileCreationException("File as non Any-File known");					
			}
		} else {
			AnyFile file = new AnyFile(name, path, type);
			AnyReader reader = new AnyReader(file);
			
			files.put(path + "/" + name + "." + type, reader);
			return new AnyFileInterface(file, reader);
		}		
	}
	
	public ArrayList<FileInterface<?, ?>> getFiles(String path) {
		ArrayList<FileInterface<?, ?>> files = new ArrayList<>();
		for(File file: new File(path).listFiles()) {
			System.out.println(file);
			if(file.isFile()) {
				try {
					int typePos = file.getName().lastIndexOf(".");
					if (typePos != 0) {
						String type = file.getName().substring(typePos + 1);
						if (type.equalsIgnoreCase("csv")) {
							files.add(getCSVFile(path, file.getName().substring(0, typePos)));
						} else if (type.equalsIgnoreCase("ktv")) {
							files.add(getKTVFile(path, file.getName().substring(0, typePos)));
						} else {
							files.add(getAnyFile(path, file.getName().substring(0, typePos), type));
						}
					}
				} catch (Throwable e) {}
			}
		}
		return files;
	}
	
	public CSVFileInterface getEncryptedCSVFile(String path, String name, String password) {		
		try {
			File f = new File(path);
			path = f.getCanonicalPath();
		} catch (IOException e) {
			throw new FileCreationException(e, "IO-Exception");
		}

		if (files.containsKey(path + "/" + name + ".csv")) {
			FileReader<?> reader = files.get(path + "/" + name + ".csv");
			if (reader instanceof EncryptedCSVReader) {
				return new CSVFileInterface(((EncryptedCSVReader)reader).getFile(), (CSVReader) reader);
			} else {
				throw new FileCreationException("File as non CSV-File known");					
			}
		} else {
			CSVFile file = new CSVFile(name, path);
			EncryptedCSVReader reader = new EncryptedCSVReader(file, password);
			
			files.put(path + "/" + name + ".csv", reader);
			return new CSVFileInterface(file, reader);
		}		
	}
	
	public KTVFileInterface getEncryptedKTVFile(String path, String name, String password) {		
		try {
			File f = new File(path);
			path = f.getCanonicalPath();
		} catch (IOException e) {
			throw new FileCreationException(e, "IO-Exception");
		}

		if (files.containsKey(path + "/" + name + ".ktv")) {
			FileReader<?> reader = files.get(path + "/" + name + ".ktv");
			if (reader instanceof EncryptedKTVReader) {
				return new KTVFileInterface(((EncryptedKTVReader)reader).getFile(), (KTVReader) reader);
			} else {
				throw new FileCreationException("File as non KTV-File known");					
			}
		} else {
			KTVFile file = new KTVFile(name, path);
			EncryptedKTVReader reader = new EncryptedKTVReader(file, password);
			
			files.put(path + "/" + name + ".ktv", reader);
			return new KTVFileInterface(file, reader);
		}		
	}
	
	public AnyFileInterface getEncryptedAnyFile(String path, String name, String type, String password) {		
		try {
			File f = new File(path);
			path = f.getCanonicalPath();
		} catch (IOException e) {
			throw new FileCreationException(e, "IO-Exception");
		}

		if (files.containsKey(path + "/" + name + "." + type)) {
			FileReader<?> reader = files.get(path + "/" + name + "." + type);
			if (reader instanceof EncryptedAnyReader && reader.getFile().getType().equals(type)) {
				return new AnyFileInterface(((EncryptedAnyReader)reader).getFile(), (AnyReader) reader);
			} else {
				throw new FileCreationException("File as non Any-File known");					
			}
		} else {
			AnyFile file = new AnyFile(name, path, type);
			EncryptedAnyReader reader = new EncryptedAnyReader(file, password);
			
			files.put(path + "/" + name + "." + type, reader);
			return new AnyFileInterface(file, reader);
		}		
	}
	
	public ArrayList<FileInterface<?, ?>> getEncryptedFiles(String path, String password) {
		ArrayList<FileInterface<?, ?>> files = new ArrayList<>();
		for(File file: new File(path).listFiles()) {
			if(file.isFile()) {
				try {
					int typePos = file.getPath().lastIndexOf(".");
					if (typePos != 0) {
						String type = file.getPath().substring(file.getPath().lastIndexOf(".") + 1);
						if (type.equalsIgnoreCase("csv")) {
							files.add(getEncryptedCSVFile(path, file.getName(), password));
						} else if (type.equalsIgnoreCase("ktv")) {
							files.add(getEncryptedKTVFile(path, file.getName(), password));
						} else {
							files.add(getEncryptedAnyFile(path, file.getName(), type, password));
						}
					}
				} catch (Throwable e) {}
			}
		}
		return files;
	}

}
