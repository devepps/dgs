package fileManager.file;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import fileManager.data.exceptions.FileReadException;
import fileManager.main.DeEnCode;

public abstract class EncryptedFileReader<FileType extends DataFile> extends FileReader<FileType> {

	private String password;
	private final DeEnCode deEnCode = new DeEnCode();

	private final ArrayList<byte[]> outData = new ArrayList<>();
	private int byteCount = 0;

	public EncryptedFileReader(FileType file, String password) {
		super(file);
		this.password = password;
	}

	@Override
	protected byte[] getFileData() {
		try {
			byte[] fileData = super.getFileData();
			if(fileData == null || fileData.length == 0) {
				return new byte[0];
			}
			return this.deEnCode.decode(fileData, password);
		} catch (UnsupportedEncodingException e) {
			throw new FileReadException(e, "File could not be decoded");
		}
	}

	@Override
	protected void write(byte[] data) {
		this.outData.add(data);
		byteCount += data.length;
	}
	
	@Override
	protected void endWrite() {
		if(byteCount > 0) {
			ByteBuffer buffer = ByteBuffer.allocate(byteCount);
			for (byte[] data : outData) {
				buffer.put(data);
			}
			buffer.rewind();
			byte[] encoded;
			try {
				encoded = deEnCode.encode(buffer.array(), password);
				super.write(encoded);
			} catch (UnsupportedEncodingException e) {
				throw new FileReadException(e, "File could not be encoded");
			} catch (IOException e) {
				throw new FileReadException(e, "IO-Exception");
			}
		}
		outData.clear();
		byteCount = 0;
		super.endWrite();
	}

}
