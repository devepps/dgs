package fileManager.file.ktv;

import fileManager.file.DataFile;
import fileManager.file.ktv.data.Tree;

public class KTVFile extends DataFile{
	
	private Tree<String> data = new Tree<>();

	public KTVFile(String name, String path) {
		super(name, path, "ktv");
		load();
	}

	public Tree<String> getData() {
		return data;
	}

}
