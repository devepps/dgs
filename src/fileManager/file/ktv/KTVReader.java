package fileManager.file.ktv;

import java.io.IOException;
import java.util.ArrayList;

import fileManager.data.exceptions.FileReadException;
import fileManager.file.FileReader;

public class KTVReader extends FileReader<KTVFile>{
	
	private static final String[] lineIn = new String[] {"    ", "	"};
	private static final String contentMark = "\"";
	private static final String keyEnd = ":";
	private static final String keySeperator = ".";

	public KTVReader(KTVFile file) {
		super(file);
	}

	@Override
	public void read() {		
		byte[] fileData = getFileData();
		if(fileData.length == 0) return;
		
		try {
			String fileContent = new String(fileData, Default_Format);
			String lineSpliter = "[";
			for (char spliter : getLineSeperators().toCharArray()) lineSpliter += spliter + "|";
			if(lineSpliter.equals("[")) {
				throw new FileReadException("Invalid Line-Seperators");
			}
			String[] lines = fileContent.split(lineSpliter.substring(0, lineSpliter.length() - 1) + "]");
			
			int lastIn = 0;
			String lastKey = "";
			for(String line: lines) {
				int currentIn = 0;
				for(String prefix: lineIn) {
					while(line.startsWith(prefix)) {
						currentIn++;
						line = line.substring(prefix.length());
					}
				}
				
				if (line.trim().equals("")) {				
				} else if (line.startsWith(contentMark)) {
					if (lastKey.equals("")) {
						System.out.println("Invalid line(No Key):" + line);				
					}else {
						String content = getContent(line);
						
						//read Content
						if (lastIn < currentIn - 1) {
							System.out.println("Invalid line: " + line);
						} else if (lastIn == currentIn - 1) {
							file.getData().add(lastKey, content);
						} else if (lastIn > currentIn - 1){
							if (currentIn >= 1){
								int diff = lastIn - (currentIn - 1);
								for (; diff > 0; diff--) {
									lastKey = lastKey.substring(0, lastKey.lastIndexOf(keySeperator));
								}
								file.getData().add(lastKey, content);
							} else {
								System.out.println("Invalid line: " + line);
							}
						}
					}
				} else {
					
					//read key
					if (lastIn < currentIn - 1) {
						System.out.println("Invalid line: " + line);						
					} else if (lastIn >= currentIn - 1) {
						String nextKey = getKey(line);
						
						if(nextKey.equals("") || nextKey.contains(keySeperator)) {					
						} else {
							int diff = lastIn - currentIn;
							for (; diff >= 0 && lastKey.lastIndexOf(keySeperator) != -1; diff--) {
								lastKey = lastKey.substring(0, lastKey.lastIndexOf(keySeperator));
							}
							if(diff >= 0) lastKey = "";
							
							lastIn = currentIn;
							if(lastKey.equals("")) {
								lastKey = nextKey;
							} else {
								lastKey = lastKey + "." + nextKey;
							}
						}
					}
				}
			}
		} catch (IOException e) {
			throw new FileReadException(e, "IO-Exception");
		}
	}

	private String getKey(String line) {
		int endPos = line.lastIndexOf(keyEnd);		
		if (endPos == -1) {
			return "";			
		}
		return line.substring(0, endPos);
	}

	private String getContent(String line) {
		int startPos = line.indexOf(contentMark);
		int endPos = line.lastIndexOf(contentMark);
		
		if (startPos == -1 || endPos == -1) {
			System.out.println("Invalid content: " + line);
			return "";
		} else if (startPos >= endPos){
			System.out.println("Invalid content: " + line);	
			return "";		
		} else if (startPos + 1 == endPos) {
			System.out.println("Empty content: " + line);	
			return "";		
		}
		
		return line.substring(startPos + 1, endPos);
	}

	@Override
	public void save() {		
		startWrite();
		for(String key : this.file.getData().getSubKey("")) {
			printToOut(key, 0);
		}
		endWrite();
	}

	private void printToOut(String key, int currentIn) {
		ArrayList<String> content = this.file.getData().get(key);
		try {
			String currentInString = "";
			for (int i = 0; i < currentIn; i++) {
				currentInString += lineIn[0];
			}
			
			int lastPoint = key.lastIndexOf(keySeperator);
			String keyLine = currentInString + key.substring(lastPoint == -1 ? 0 : lastPoint + 1) + keyEnd + getLineSeperators().charAt(0);
			
			write(keyLine.getBytes(Default_Format));
			if (content != null && !content.isEmpty()) {
				for(int i = 0; i < content.size(); i++) {
					String contentLine = content.get(i);
					contentLine = contentLine == null ? "" : contentLine;
					
					try{
						write((currentInString + lineIn[0] + contentMark + contentLine + contentMark  + getLineSeperators().charAt(0)).getBytes(Default_Format));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String[] subKeys = this.file.getData().getSubKey(key);
		if (subKeys != null && subKeys.length > 0) {
			for(String subKey : subKeys) {
				printToOut(key + "." + subKey, currentIn + 1);
			}
		}
	}

}
