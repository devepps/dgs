package fileManager.file.ktv;

import java.util.ArrayList;

import fileManager.file.FileInterface;
import fileManager.file.FileReader;
import fileManager.file.ktv.data.Node;

public class KTVFileInterface extends FileInterface<KTVFile, FileReader<KTVFile>>{

	public KTVFileInterface(KTVFile file, FileReader<KTVFile> reader) {
		super(file, reader);
	}
	
	public void add(String key, String... obj){
		file.getSyncManager().syncronize(() ->  file.getData().add(key, obj));
	}

	public void remove(String key, String... obj) {
		if (obj.length == 0) {
			file.getSyncManager().syncronize(() -> file.getData().remove(key));
		} else {
			for (String s : obj) {
				file.getSyncManager().syncronize(() -> file.getData().remove(key, s));
			}
		}
	}

	public ArrayList<String> get(String key) {
		ArrayList<String> values = file.getSyncManager().syncronize(() -> file.getData().get(key));
		return values == null ? new ArrayList<String>() : values;
	}
	
	public void set(String key, String... value){
		file.getSyncManager().syncronize(() -> file.getData().set(key, value));
	}
	
	public String[] getSubkey(String key){
		String[] subKeys = file.getSyncManager().syncronize(() -> file.getData().getSubKey(key));
		return subKeys == null ? new String[0] : subKeys;
	}

	@Override
	public void save() {
		file.getSyncManager().syncronize(() -> reader.save());
	}

	@Override
	public void load() {
		file.getSyncManager().syncronize(() -> reader.read());
	}

	public ArrayList<Node<String>> getStartNodes() {
		ArrayList<Node<String>> startKeys = file.getSyncManager().syncronize(() -> file.getData().getStartNodes());
		return startKeys == null ? new ArrayList<Node<String>>() : startKeys;
	}

}
