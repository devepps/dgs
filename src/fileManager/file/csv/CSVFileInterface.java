package fileManager.file.csv;

import java.awt.Point;

import fileManager.file.FileInterface;
import fileManager.file.FileReader;

public class CSVFileInterface extends FileInterface<CSVFile, FileReader<CSVFile>> {

	public CSVFileInterface(CSVFile file, FileReader<CSVFile> reader) {
		super(file, reader);
	}

	public void set(Point p, String data) {
		file.getSyncManager().syncronize(() -> file.getDataManager().set(p, data));

	}

	public String get(Point p) {
		return file.getSyncManager().syncronize(() -> file.getDataManager().get(p));
	}

	public void remove(Point p) {
		file.getSyncManager().syncronize(() -> file.getDataManager().remove(p));
	}

	public void removeLine(int y, boolean moveOthers) {
		file.getSyncManager().syncronize(() -> file.getDataManager().removeLine(y, moveOthers));
	}

	public void removeColumn(int x, boolean moveOthers) {
		file.getSyncManager().syncronize(() -> file.getDataManager().removeColumn(x, moveOthers));
	}

	public Point getmaxPos() {
		return file.getSyncManager().syncronize(() -> file.getDataManager().getMaxPos());
	}

	@Override
	public void save() {
		file.getSyncManager().syncronize(() -> reader.save());
	}

	@Override
	public void load() {
		file.getSyncManager().syncronize(() -> reader.read());
	}

}
