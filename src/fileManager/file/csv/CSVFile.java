package fileManager.file.csv;
import fileManager.file.DataFile;
import fileManager.file.csv.data.CSVDataManager;

public class CSVFile extends DataFile {

	private CSVDataManager dataManager = new CSVDataManager();

	public CSVFile(String name, String path) {
		super(name, path, "csv");
		load();
	}

	public CSVDataManager getDataManager() {
		return dataManager;
	}
}
