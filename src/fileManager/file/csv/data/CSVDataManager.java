package fileManager.file.csv.data;

import java.awt.Point;
import java.util.HashMap;

public class CSVDataManager {

	private final HashMap<Point, String> content = new HashMap<>();
	private Point maxPos = new Point(-1, -1);
	
	public void set(Point p, String data) {
		content.put(p, data);
		if (p.getX() > maxPos.getX()) {
			maxPos.x = (int) p.getX();
		}
		if (p.getY() > maxPos.getY()) {
			maxPos.y = (int) p.getY();
		}
	}

	public String get(Point p) {
		if (!content.containsKey(p)) {
			return "";
		}
		return content.get(p);
	}

	public void remove(Point p) {
		content.remove(p);
		if (p.getX() == maxPos.getX()) {
			maxPos.x--;
		}
		if (p.getY() == maxPos.getY()) {
			maxPos.y--;
		}
	}

	public void removeLine(int y, boolean moveOthers) {
		if (y < 0 || y > maxPos.getY()) {
			return;
		}

		for (int x = 0; x <= maxPos.getX(); x++) {
			content.remove(new Point(x, y));
		}
		if (y == maxPos.getY()) {
			maxPos.y--;
		} else if (moveOthers) {
			for (int dy = y + 1; dy <= maxPos.getY(); dy++) {
				if (dy > 0) {
					for (int x = 0; x <= maxPos.getX(); x++) {
						Point p = new Point(x, dy);
						String data = content.get(p);
						content.remove(p);
						content.put(new Point(x, dy - 1), data);
					}
				}
			}
			maxPos.y--;
		}
	}

	public void removeColumn(int x, boolean moveOthers) {
		if (x < 0 || x > maxPos.getX()) {
			return;
		}
		for (int y = 0; y <= maxPos.getY(); y++) {
			content.remove(new Point(x, y));
		}
		if (x == maxPos.getX()) {
			maxPos.x--;
		} else if (moveOthers) {
			for (int dx = x + 1; dx <= maxPos.getX(); dx++) {
				if (dx > 0) {
					for (int y = 0; y <= maxPos.getY(); y++) {
						Point p = new Point(dx, y);
						String data = content.get(p);
						content.remove(p);
						content.put(new Point(dx - 1, y), data);
					}
				}
			}
			maxPos.y--;
		}
	}

	public Point getMaxPos() {
		return (Point) maxPos.clone();
	}

}
