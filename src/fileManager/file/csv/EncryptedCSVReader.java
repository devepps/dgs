package fileManager.file.csv;

import java.awt.Point;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fileManager.data.exceptions.FileReadException;
import fileManager.file.EncryptedFileReader;

public class EncryptedCSVReader extends EncryptedFileReader<CSVFile> {
	
	private static final String seperator = ";";

	public EncryptedCSVReader(CSVFile file, String password) {
		super(file, password);
	}

	@Override
	public void read() {		
		byte[] fileData = getFileData();
		if(fileData.length == 0) return;
		
		try {
			String content = new String(fileData, Default_Format);
			String spliter = "[";
			for (char seperator : getLineSeperators().toCharArray()) spliter += seperator + "|";
			if(spliter.equals("[")) {
				throw new FileReadException("Invalid Line-Seperators");
			}
			String[] lines = content.split(spliter.substring(0, spliter.length() - 1) + "]");
			
			int y = 0;
			for(String line: lines) {
				String[] cells = line.split(seperator);
				for(int x = 0; x < cells.length; x++) {
					file.getDataManager().set(new Point(x, y), cells[x]);					
				}
				y++;
			}
		} catch (IOException e) {
			throw new FileReadException(e, "IO-Exception");
		}
	}

	@Override
	public void save() {	
		startWrite();	
		Point maxPos = file.getDataManager().getMaxPos();
		for(int y = 0; y <= maxPos.getY() ; y++) {
			for(int x = 0; x <= maxPos.getX(); x++) {
				String content = file.getDataManager().get(new Point(x, y));
				try {
					byte[] data = (content + seperator).getBytes(Default_Format);
					write(data);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			try {
				write((getLineSeperators().charAt(0) + "").getBytes(Default_Format));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		endWrite();
	}

}
