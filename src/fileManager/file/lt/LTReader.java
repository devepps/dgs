package fileManager.file.lt;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fileManager.file.FileReader;

public class LTReader extends FileReader<LTFile>{

	public LTReader(LTFile file) {
		super(file);
	}

	@Override
	public void read() {
		byte[] fileData = getFileData();
		if(fileData.length == 0) return;
		
		try {
			String fileContent = new String(fileData, Default_Format);
			file.setText(fileContent);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void save() {		
		startWrite();
		try {
			write(file.getText().getBytes(Default_Format));
		} catch (IOException e) {
			e.printStackTrace();
		}
		endWrite();
	}

}
