package fileManager.file.lt;

import fileManager.file.DataFile;

public class LTFile extends DataFile{

	private String text = "";
	
	public LTFile(String name, String path) {
		super(name, path, "lt");
		load();
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

}
