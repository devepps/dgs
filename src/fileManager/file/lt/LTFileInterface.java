package fileManager.file.lt;

import fileManager.file.FileInterface;

public class LTFileInterface extends FileInterface<LTFile, LTReader> {

	public LTFileInterface(LTFile file, LTReader reader) {
		super(file, reader);
	}
	
	public void write(String text) {
		file.getSyncManager().syncronize(() -> file.setText(file.getText() + text));
	}
	
	public void writeln(String text) {
		file.getSyncManager().syncronize(() -> file.setText(file.getText() + text + "\n"));
	}
	
	public void newLine() {
		file.getSyncManager().syncronize(() -> file.setText(file.getText() + "\n"));	
	}

	@Override
	public void save() {
		file.getSyncManager().syncronize(() -> reader.save());
	}

	@Override
	public void load() {
		file.getSyncManager().syncronize(() -> reader.read());
	}

}
