package fileManager.file;

public abstract class FileInterface<FileType extends DataFile, ReaderType extends FileReader<?>> {

	protected FileType file;
	protected ReaderType reader;
	
	public FileInterface(FileType file, ReaderType reader) {
		super();
		this.file = file;
		this.reader = reader;
	}
	
	public abstract void save();
	public abstract void load();
	
	public String getName() {
		return file.getName();
	}
	
	public String getPath() {
		return file.getPath();
	}
	
	public String getAbsolutePath() {
		return file.getAbsolutePath();
	}
	
	public boolean hasExisted() {
		return file.hasFileExistetd();
	}
	
}