package fileManager.file;

import java.io.File;
import java.io.IOException;

import collection.sync.SyncManager;
import fileManager.data.exceptions.FileCreationException;

public abstract class DataFile {

	private File file;
	protected SyncManager syncManager = new SyncManager();

	private String name;
	private String path;
	private String type;
	private boolean fileExistetd;

	public DataFile(String name, String path, String type) {
		this.name = name;
		this.path = path;
		this.type = type;
		
		this.file = new File(path + "/" + name + "." + type);		
		fileExistetd = file.exists();
	}

	public String getType() {
		return type;
	}

	public void load() {
		if (!file.exists()) {
			new File(path).mkdirs();

			try {
				file.createNewFile();
			} catch (IOException e) {
				throw new FileCreationException(e, "IO-Error");
			}
		}
	}

	public String getName() {
		return name;
	}

	public String getPath() {
		return path;
	}

	public String getAbsolutePath() {
		return file.getAbsolutePath();
	}

	public File getFile() {
		return file;
	}

	public SyncManager getSyncManager() {
		return syncManager;
	}

	public boolean hasFileExistetd() {
		return fileExistetd;
	}
}
