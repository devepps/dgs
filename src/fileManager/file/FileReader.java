package fileManager.file;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;

import fileManager.data.exceptions.FileReadException;
import fileManager.data.exceptions.FileSaveException;

public abstract class FileReader<FileType extends DataFile> {

	protected static final String Default_Format = "ISO_8859_1";

	protected FileType file;

	private FileOutputStream out;

	public FileReader(FileType file) {
		this.file = file;
	}

	public abstract void read();

	public abstract void save();

	public FileType getFile() {
		return file;
	}

	protected byte[] getFileData() {
		if (!file.getFile().exists()) {
			throw new FileReadException("File does not exist");
		} else if (!file.getFile().canRead()) {
			throw new FileReadException("File can not be read");
		}

		try {
			return Files.readAllBytes(file.getFile().toPath());
		} catch (FileNotFoundException e) {
			throw new FileReadException(e, "File can not be found");
		} catch (IOException e) {
			throw new FileReadException(e, "IO-Exception");
		} catch (OutOfMemoryError e) {
			throw new FileReadException("File to large");
		} catch (SecurityException e) {
			throw new FileReadException(e, "File is locked");
		}
	}

	protected void write(byte[] data) throws IOException {
		if (out == null) {
			throw new FileSaveException("File can not be rewritten");
		} else if (!file.getFile().exists()) {
			throw new FileSaveException("File does not exist");
		} else if (!file.getFile().canWrite()) {
			throw new FileSaveException("File can not be rewritten");
		}

		out.write(data);
	}

	protected int getByteCount(String format) {
		try {
			return "0".getBytes(format).length;
		} catch (UnsupportedEncodingException e) {
			return 0;
		}
	}

	protected int getByteCount() {
		return getByteCount(Default_Format);
	}

	protected String getLineSeperators() {
		return System.getProperty("line.separator");
	}
	
	protected void startWrite() {
		try {
			if(out == null) {
				this.out = new FileOutputStream(file.getFile());
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	protected void endWrite() {
		try {
			out.flush();
			out.getChannel().position(0);
		} catch (Throwable e) {
			throw new FileSaveException("File can not be rewritten");
		}
	}
}
