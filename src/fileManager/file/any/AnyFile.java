package fileManager.file.any;

import fileManager.file.DataFile;

public class AnyFile extends DataFile{
	
	private byte[] data;

	public AnyFile(String name, String path, String type) {
		super(name, path, type);
		load();
	}
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

}
