package fileManager.file.any;

import fileManager.file.EncryptedFileReader;

public class EncryptedAnyReader extends EncryptedFileReader<AnyFile> {

	public EncryptedAnyReader(AnyFile file, String password) {
		super(file, password);
	}

	@Override
	public void read() {
		byte[] data = getFileData();
		file.setData(data);
	}

	@Override
	public void save() {
		startWrite();

		write(file.getData());

		endWrite();
	}

}
