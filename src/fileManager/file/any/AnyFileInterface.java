package fileManager.file.any;

import fileManager.file.FileInterface;
import fileManager.file.FileReader;

public class AnyFileInterface extends FileInterface<AnyFile, FileReader<AnyFile>> {

	public AnyFileInterface(AnyFile file, FileReader<AnyFile> reader) {
		super(file, reader);
	}

	@Override
	public void save() {
		file.getSyncManager().syncronize(() -> reader.save());
	}

	@Override
	public void load() {
		file.getSyncManager().syncronize(() -> reader.read());
	}

	public byte[] getData() {
		return file.getSyncManager().syncronize(() -> file.getData());
	}

	public void setData(byte[] data) {
		file.getSyncManager().syncronize(() -> file.setData(data));
	}
}
