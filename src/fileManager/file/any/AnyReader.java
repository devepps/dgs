package fileManager.file.any;

import java.io.IOException;

import fileManager.data.exceptions.FileSaveException;
import fileManager.file.FileReader;

public class AnyReader extends FileReader<AnyFile>{

	public AnyReader(AnyFile file) {
		super(file);
	}

	@Override
	public void read() {
		file.setData(getFileData());
	}

	@Override
	public void save() {
		startWrite();
		try {
			write(file.getData());
		} catch (IOException e) {
			throw new FileSaveException(e, "IO-Exception");
		}
		endWrite();
	}

}
