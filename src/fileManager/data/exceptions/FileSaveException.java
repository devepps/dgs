package fileManager.data.exceptions;

import data.exceptions.CustomException;

public class FileSaveException extends CustomException{

	private static final long serialVersionUID = 1L;

	public FileSaveException(Exception exception, String reason) {
		super(exception, "File could not be saved. Reason: " + reason);
	}
	
	public FileSaveException(String reason) {
		super(null, "File could not be saved. Reason: " + reason);
	}

}
