package fileManager.data.exceptions;

import data.exceptions.CustomException;

public class FileCreationException extends CustomException{

	private static final long serialVersionUID = 1L;

	public FileCreationException(Exception exception, String reason) {
		super(exception, "File could not be created. Reason: " + reason);
	}
	
	public FileCreationException(String reason) {
		super(null, "File could not be created. Reason: " + reason);
	}

}
