package fileManager.data.exceptions;

import data.exceptions.CustomException;

public class FileReadException extends CustomException{

	private static final long serialVersionUID = 1L;

	public FileReadException(Exception exception, String reason) {
		super(exception, "File could not be read. Reason: " + reason);
	}
	
	public FileReadException(String reason) {
		super(null, "File could not be read. Reason: " + reason);
	}

}
