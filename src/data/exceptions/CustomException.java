package data.exceptions;

public class CustomException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String message;
	private Throwable exception;

	private long created;

	public CustomException(Throwable e, String message) {
		this.message = message + (e == null ? "" : " : " + e.getMessage());
		this.exception = e;
		this.created = System.currentTimeMillis();
	}

	public CustomException(String message) {
		this.message = message + " : ";
		this.created = System.currentTimeMillis();
	}

	public String getErrorMessage() {
		return this.message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	public Throwable getException() {
		return exception;
	}

	public long getCreatedTime() {
		return created;
	}

}
