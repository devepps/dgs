package data.queue;

public interface Action<ContentType> {
	
	public enum ActionType {
		NONE, REMOVE, STOP, REMOVE_STOP;
	}

	public ActionType act(ContentType host);

}
