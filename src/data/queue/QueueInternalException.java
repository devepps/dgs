package data.queue;

import data.exceptions.CustomException;

public class QueueInternalException extends CustomException {

	private static final long serialVersionUID = 1L;

	public QueueInternalException() {
		super("Internal Error in Queue");
	}		
}
