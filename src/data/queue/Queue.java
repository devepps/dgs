package data.queue;

import collection.interceptor.Interceptor;
import data.queue.Action.ActionType;

public class Queue<ContentType> {

	private class Node {
		ContentType Object;
		Node next;
		Node before;

		public Node(ContentType Object) {
			this.Object = Object;
		}
	}

	private Node head = null;
	private Node tail = null;

	private int length = 0;

	public void add(ContentType Object) {
		Node n = new Node(Object);
		if (tail != null) {
			tail.next = n;
			n.before = tail;
		}
		tail = n;
		if (head == null) {
			head = n;
		}

		length++;
	}

	public ContentType get() {
		return head.Object;
	}

	public void remove() {
		if (head != null) {
			removeNode(head);
		}
	}

	public boolean isEmpty() {
		return head == null;
	}

	public Queue<ContentType> clone() {
		Queue<ContentType> clone = new Queue<>();
		Node node = head;
		while (node != null) {
			clone.add(node.Object);
			node = node.next;
		}
		return clone;
	}

	public int getLength() {
		return length;
	}

	public ContentType pop() {
		ContentType obj = get();
		remove();
		return obj;
	}

	public void clear() {
		this.head = null;
		this.tail = null;
		this.length = 0;
	}

	public void actOnContent(Action<ContentType> action) {
		this.actOnContent(action, head);
	}

	public void actOnContent(Action<ContentType> action, Interceptor<Object> interceptor) {
		@SuppressWarnings("unchecked")
		Node head = (Queue<ContentType>.Node) interceptor.intercept(() -> this.head);
		this.actOnContent(action, head, interceptor);
	}

	private void actOnContent(Action<ContentType> action, Node currentNode) {
		if (currentNode == null) {
			return;
		}

		ActionType type = null;
		try {
			type = action.act(currentNode.Object);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		switch (type) {
			case NONE:
				break;
			case REMOVE:
				removeNode(currentNode);
				break;
			case REMOVE_STOP:
				removeNode(currentNode);
				return;
			case STOP:
				return;
			default:
				break;
		}

		actOnContent(action, currentNode.next);
	}

	private void actOnContent(Action<ContentType> action, Node currentNode, Interceptor<Object> interceptor) {
		if (currentNode == null) {
			return;
		}

		ActionType type = null;
		try {
			@SuppressWarnings("unchecked")
			ContentType content = (ContentType) interceptor.intercept(() -> currentNode.Object);
			type = action.act(content);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		switch (type) {
			case NONE:
				break;
			case REMOVE:
				removeNode(currentNode);
				break;
			case REMOVE_STOP:
				removeNode(currentNode);
				return;
			case STOP:
				return;
			default:
				break;
		}

		@SuppressWarnings("unchecked")
		Node nextNode = (Queue<ContentType>.Node) interceptor.intercept(() -> currentNode.next);
		actOnContent(action, nextNode);
	}

	private void removeNode(Node currentNode) {
		if(currentNode != null) {
			if(currentNode.before == null) {
				if(head != currentNode) {
					throw new QueueInternalException();
				} else {
					head = head.next;
					if (head != null) {
						head.before = null;
					}
				}
			} else {
				currentNode.before.next = currentNode.next;
				if (currentNode.next != null) {
					currentNode.next.before = currentNode.before;
				}
			}

			if (currentNode == tail || head == null) {
				tail = null;
			}
			length = (length > 0) ? (length - 1) : 0;
		}
	}

	public String toString() {
		return "Queue[" + readQueueBody(head) + "]";
	}
	
	private String readQueueBody(Node currentNode) {
		if (currentNode == null) {
			return "";
		}
		return "{" + currentNode.Object + "}, " + readQueueBody(currentNode.next);
	}
}
