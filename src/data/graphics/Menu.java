package data.graphics;

import javax.swing.JPanel;

public abstract class Menu {
	
	protected JPanel frame;
	
	public Menu(int w, int h) {
		frame = new JPanel();
		frame.setLayout(null);
		frame.setSize(w, h);
	}
	
	public abstract void generateVisuals();
	public abstract void loadFunctions();
	
	public JPanel toGraphics() {
		return frame;
	}

}
