package data.graphics.gui;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class InputHint {

	private JLabel label;
	private FocusListener focusListener;
	private MouseListener mouseListener;
	private JComponent superComponent;

	public InputHint(String text, JComponent superComponent) {
		this.superComponent = superComponent;

		label = new JLabel(text, SwingConstants.CENTER);
		label.setBounds(superComponent.getX() + 5, superComponent.getY() + 5, superComponent.getWidth() - 5,
				superComponent.getHeight() - 5);
		label.setForeground(Color.GRAY);
		label.setFont(GUI.DEFAULT_FONT);

		this.mouseListener = new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseExited(MouseEvent e) {}
			@Override
			public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseClicked(MouseEvent e) {
				Point p = e.getPoint();
				p.y -= 5;
				if (label.contains(p)) {
					destroyVisuals();
				}
			}

		};
		this.focusListener = new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {}
			@Override
			public void focusGained(FocusEvent e) {
				destroyVisuals();
			}
		};

		label.addFocusListener(focusListener);
		label.addMouseListener(mouseListener);
		superComponent.addFocusListener(focusListener);
		superComponent.addMouseListener(mouseListener);
	}

	private void destroyVisuals() {
		label.setText("");
		label.setVisible(false);
		superComponent.removeFocusListener(focusListener);
		superComponent.removeMouseListener(mouseListener);
		label.removeFocusListener(focusListener);
		label.removeMouseListener(mouseListener);
	}

	public JLabel getGraphics() {
		return label;
	}
}
