package data.graphics.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;

public abstract class GUI <Host> {
	
	public static final Font DEFAULT_FONT = new Font("TimesNewRoman", Font.BOLD, 20);
	public static final int offSetX = 15;
	public static final int offSetY = 38;

	protected JFrame window;

	private Dimension size;

	protected Host guiManger;

	protected GUI(Host guiManger, Dimension size){
		this.guiManger = guiManger;
		this.size = size;
		
		generateVisuals();
		
		loadFunctions();
		
		setKillAble();
		
		update();
	}

	protected void loadFunctions() {		
		this.window.addComponentListener(new ComponentListener() {

			HashMap<Component, int[]> originalSize = new HashMap<>();
			
			public void componentShown(ComponentEvent e) {}
			public void componentMoved(ComponentEvent e) {}
			public void componentHidden(ComponentEvent e) {}
			
			@Override
			public void componentResized(ComponentEvent e) {
				double dw = window.getWidth() /size.getWidth();
				double dh = window.getHeight()/size.getHeight();
				
				updateSize(dw, dh, window.getContentPane().getComponents());				
				
			}

			private void updateSize(double dw, double dh, Component[] components) {
				for(Component comp: components){
					if(originalSize.containsKey(comp)){
						int[] compdata = originalSize.get(comp);
						comp.setSize((int) (compdata[2] * dw), (int) (compdata[3] * dh));
						comp.setLocation((int) (compdata[0] * dw), (int) (compdata[1] * dh));
					}else{
						originalSize.put(comp, new int[]{comp.getX(), comp.getY(), comp.getWidth(), comp.getHeight()});
					}
					
					if(comp instanceof JPanel){
						updateSize(dw, dh, ((JPanel)comp).getComponents());
					}
				}
			}
		});
	}

	protected void update() {
		this.window.setSize((int) window.getWidth()+1, (int) window.getHeight()+1);
		this.window.setSize(window.getSize());
	}

	protected void generateVisuals() {
		this.window = new JFrame("Client");
		this.window.setSize(size);
		this.window.setVisible(true);
		this.window.setLayout(null);
		this.window.getContentPane().setBackground(new Color(160, 160, 160));	
	}

	/**
	 * enables the program to kill every thread ones the window is closed
	 */
	protected void setKillAble(){
		this.window.addWindowListener(new WindowListener() {
			public void windowOpened(WindowEvent e) {}
			public void windowIconified(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowDeactivated(WindowEvent e) {}
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
			public void windowClosed(WindowEvent e) {}
			public void windowActivated(WindowEvent e) {}
		});
	}

	public void hide() {
		window.setVisible(false);
	}

	public void show() {
		window.setVisible(true);
	}

}
