package logger.test;

import logger.main.Logger;
import logger.main.prefixs.TextPrefix;
import logger.main.prefixs.TimePrefix;

public class TestLogger extends Logger{
	
	public static void main(String[] args) {
		new TestLogger();
	}
	
	public TestLogger() {
		super("test", new TextPrefix("Test"), new TimePrefix("dd.MM.yy"), new TimePrefix("hh:mm:ss"));
		
		print("test1");
		println("test2");
		println("test3");
	}

}
