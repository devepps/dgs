package logger.main.prefixs;

public class TextPrefix extends Prefix {
	
	private String prefix;

	public TextPrefix(String prefix) {
		super();
		this.prefix = prefix;
	}
	@Override
	public String getPrefix() {
		return prefix;
	}

}
