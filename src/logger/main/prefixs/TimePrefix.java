package logger.main.prefixs;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimePrefix extends Prefix{

	private String pattern;
	
	public TimePrefix(String pattern) {
		super();
		this.pattern = pattern;
	}

	@Override
	public String getPrefix() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern(pattern));
	}

}
