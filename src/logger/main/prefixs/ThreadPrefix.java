package logger.main.prefixs;

public class ThreadPrefix extends Prefix {

	@Override
	public String getPrefix() {
		return Thread.currentThread().getName();
	}
	
}
