package logger.main;

import java.time.LocalDate;

import collection.sync.SyncQueue;
import fileManager.file.lt.LTFileInterface;
import fileManager.main.FileManager;
import logger.main.prefixs.Prefix;

public class Logger {

	private final SyncQueue<String> logText = new SyncQueue<>();
	private final FileManager fileManager = new FileManager();

	private LTFileInterface currentFile;
	private String lastText = "";
	
	private Prefix[] prefixes;
	
	public Logger(String name, Prefix... prefixes) {
		this.prefixes = prefixes;
		int i = 0;
		do {
			currentFile = fileManager.getLTFile("log", name + " - " + LocalDate.now().getDayOfMonth() + "-"
					+ LocalDate.now().getMonthValue() + "-" + LocalDate.now().getYear() + "-" + i);
			i++;
		} while (currentFile.hasExisted());

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					while (!logText.isEmpty()) {
						String log = "[" + name + "] " + logText.get();
						logText.remove();

						System.out.println(log);
						currentFile.writeln(log);
					}
					
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					currentFile.save();
				}
			}
		}).start();
	}

	public void print(String text) {
		String[] lines = getLines(text == null ? "null" : text);
		for (int i = lines.length - 2; i >= 0; i--) {
			addToLogText(lines[i]);
		}
		lastText += lines[lines.length - 1];
	}

	private void addToLogText(String logLine) {
		for (int i = prefixes.length - 1; i >= 0; i--) {
			logLine = "[" + prefixes[i].getPrefix() + "] " + logLine;
		}
		logText.add(logLine);
	}

	private String[] getLines(String text) {
		String lineSpliter = "[";
		for (char spliter : System.getProperty("line.separator").toCharArray()) {
			lineSpliter += spliter + "|";
		}
		return text.split(lineSpliter.substring(0, lineSpliter.length() - 1) + "]");
	}

	public void println(String text) {
		text = lastText + text;
		String[] lines = getLines(text == null ? "null" : text);
		for (int i = 0; i < lines.length; i++) {
			addToLogText(lines[i]);
		}
		lastText = "";
	}
}
